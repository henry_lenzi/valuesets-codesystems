ValueSet: Estado_da_Resolução_de_Diagnóstico_ou_Problema.
Id: br-core-condition-clinicalstatus
Description: "Estado da resolução de um diagnóstico ou problema."
* ^name = "Estado da Resolução de Diagnóstico ou Problema."
* ^meta.lastUpdated = "2020-03-12T13:09:54.402+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BREstadoResolucaoDiagnosticoProblema-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:09:53.3447193+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* $condition-clinical#active "Não Resolvido ou Resolvendo"
* $condition-clinical#resolved "Resolvido"