ValueSet: Estado_do_Evento
Id: 2fc05498-136e-40fc-8cd7-0107fc263cb4
Description: "Identificação do estado de um evento."
* ^name = "Estado do Evento"
* ^meta.lastUpdated = "2020-04-07T12:14:10.395+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BREstadoEvento-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-04-07T12:14:07.8417018+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* EventStatus#preparation "Solicitado"
* EventStatus#on-hold "Suspenso"
* EventStatus#completed "Concluído"
* EventStatus#entered-in-error "Cancelado por informação errada"