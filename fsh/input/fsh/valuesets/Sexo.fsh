ValueSet: Sexo
Id: d50deb66-f886-4702-9c21-2ca1bf4496eb
Description: "Sexo de um indivíduo."
* ^meta.lastUpdated = "2020-04-07T21:07:03.610+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRSexo-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-04-07T21:07:00.8812267+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* AdministrativeGender#male "Masculino"
* AdministrativeGender#female "Feminino"
* AdministrativeGender#unknown "Ignorado"