ValueSet: Unidade_Federativa
Id: br-core-address-state
Description: "Os estados e o Distrito Federal, entes subnacionais brasileiros."
* ^name = "Unidade Federativa"
* ^meta.lastUpdated = "2020-03-12T14:51:21.573+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRUnidadeFederativa-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T14:51:21.2223533+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRDivisaoGeograficaBrasil|*
    where type = "state"