ValueSet: Tipo_de_Observação
Id: 5cca3c66-0ea2-4b0c-ba9c-8da8afee6cc5
Description: "Tipo de Observação."
* ^name = "Tipo de Observação"
* ^meta.lastUpdated = "2020-06-09T17:08:32.522+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRTipoObservacao-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-06-09T17:08:58.0863667+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRTipoObservacao|*
* $BRTabelaSUS#0301100039