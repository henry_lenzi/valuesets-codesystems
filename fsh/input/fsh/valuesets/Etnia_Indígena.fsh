ValueSet: Etnia_Indígena
Id: br-core-patient-raceethnicity-indigenousethnicity
Description: "Códigos para representação de etnias indígenas no Brasil."
* ^name = "Etnia Indígena"
* ^meta.lastUpdated = "2020-03-12T13:10:19.911+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BREtniaIndigena-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:10:19.0007937+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system $BREtniaIndigena