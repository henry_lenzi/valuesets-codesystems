ValueSet: Motivo_do_desfecho_do_Contato_assistencial
Id: br-core-encounter-hospitalization-dischargedisposition
Description: "ValueSet utilizado para classificar o motivo de conclusão total ou parcial do contato assistencial."
* ^name = "Motivo do desfecho do Contato assistencial"
* ^meta.lastUpdated = "2020-03-12T13:18:05.304+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRMotivoDesfecho-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:18:04.0945614+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRMotivoDesfecho|*