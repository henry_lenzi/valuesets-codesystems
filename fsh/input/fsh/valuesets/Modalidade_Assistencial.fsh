ValueSet: Modalidade_Assistencial
Id: br-core-encounter-class
Description: "Classificação dos documentos e contatos assistenciais de acordo com as especificidades do modo, local e duração do atendimento."
* ^name = "Modalidade Assistencial"
* ^meta.lastUpdated = "2020-03-12T13:17:22.715+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRModalidadeAssistencial-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:17:18.5012669+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRModalidadeAssistencial|*