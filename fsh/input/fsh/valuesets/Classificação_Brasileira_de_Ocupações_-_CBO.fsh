ValueSet: Classificação_Brasileira_de_Ocupações_-_CBO
Id: br-core-cbo
Description: "Classifica as profissões do mercado de trabalho brasileiro."
* ^name = "Classificação Brasileira de Ocupações - CBO"
* ^meta.lastUpdated = "2020-03-12T13:22:07.223+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BROcupacao-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:22:05.8261152+00:00"
* ^publisher = "Ministério do Trabalho e Emprego do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRCBO|*