ValueSet: Resultado_Qualitativo_do_Exame
Id: 04e15b9e-7dc5-4455-ae70-d3f8e677b7f8
Description: "ValueSet utilizado para definir o valor atribuído ao resultado de um exame laboratorial realizado por método de análise qualitativo."
* ^name = "Resultado Qualitativo do Exame"
* ^meta.lastUpdated = "2020-03-26T13:21:39.798+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRResultadoQualitativoExame-1.0"
* ^version = "201701A"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-26T13:21:38.6543319+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRResultadoQualitativoExame|*