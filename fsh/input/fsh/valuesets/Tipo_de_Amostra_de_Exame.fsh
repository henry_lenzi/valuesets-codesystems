ValueSet: Tipo_de_Amostra_de_Exame
Id: 49458d7d-2bf6-40fc-a0f3-e03f5ff11a36
Description: "Tipo da amostra de um exame ou teste."
* ^name = "Tipo de Amostra de Exame"
* ^meta.lastUpdated = "2020-06-30T18:56:50.208+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRTipoAmostra-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-06-30T18:56:47.6561959+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* $v2-0487#FRS "Fluido Respiratório"
* $v2-0487#LAVG "Lavado Brônquico"
* $v2-0487#NSECR "Secreção Nasal"
* $v2-0487#PLAS "Plasma"
* $v2-0487#SPT "Escarro"
* $v2-0487#TASP "Aspirado Traqueal"
* $v2-0487#WB "Sangue Total"
* $v2-0487#SER "Soro"
* $v2-0487#SECRE "Secreção Não Especificada"
* $v2-0487#ASP "Aspirado Não Especificado"
* $v2-0487#SPUTIN "Escarro induzido"
* $v2-0487#WASH "Lavado"
* $v2-0487#CSF "Líquor"
* $v2-0487#SAL "Saliva"
* $v2-0487#UR "Urina"
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRTipoAmostraGAL|*