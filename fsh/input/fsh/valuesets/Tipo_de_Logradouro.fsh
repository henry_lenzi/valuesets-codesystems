ValueSet: Tipo_de_Logradouro
Id: br-core-address-line-streettype
Description: "Classifica o tipo de logradouro."
* ^name = "Tipo de Logradouro"
* ^meta.lastUpdated = "2020-03-12T14:50:48.259+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRTipoLogradouro-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T14:50:47.8940392+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRTipoLogradouro|*