ValueSet: Tipo_de_Estabelecimento_de_Saúde
Id: br-core-organization-type
Description: "ValueSet utilizado para classificar o tipo de estabelecimento de saúde."
* ^name = "Tipo de Estabelecimento de Saúde"
* ^meta.lastUpdated = "2020-03-12T13:27:41.424+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRTipoEstabelecimentoSaude-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:27:40.6996671+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRTipoEstabelecimentoSaude|*