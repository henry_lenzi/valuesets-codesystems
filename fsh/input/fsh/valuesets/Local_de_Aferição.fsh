ValueSet: Local_de_Aferição
Id: br-core-observation-bodysite
Description: "ValueSet utilizado para identificar a parte do corpo utilizada para aferir a pressão arterial."
* ^name = "Local de Aferição"
* ^meta.lastUpdated = "2020-03-12T13:11:09.671+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRLocalAfericao-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:11:08.642377+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRLocalAfericao|*