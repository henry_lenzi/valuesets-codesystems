ValueSet: Procedimento_realizado
Id: 5cee5980-4c03-4b86-8a2a-85006a28bc38
Description: "Value Set das classificações brasileiras para procedimentos adotadas em contexto nacional."
* ^name = "Procedimento realizado"
* ^meta.lastUpdated = "2020-04-07T18:23:28.563+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRProcedimentosNacionais-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-04-07T18:23:26.5540616+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRCBHPMTUSS|*
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRTabelaSUS|*