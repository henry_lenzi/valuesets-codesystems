ValueSet: Categoria_da_observação
Id: br-core-observation-category
Description: "Classifica a observação clínica realizada, conforme especificado no padrão FHIR."
* ^name = "Categoria da observação"
* ^meta.lastUpdated = "2020-06-09T17:07:02.242+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRCategoriaObservacao-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-06-09T17:07:24.6148832+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* $observation-category#social-history "História social"
* $observation-category#vital-signs "Sinais vitais"
* $observation-category#imaging "Imagem"
* $observation-category#laboratory "Laboratório"
* $observation-category#procedure "Procedimento"
* $observation-category#survey "Avaliação"
* $observation-category#exam "Exame físico"
* $observation-category#therapy "Terapia"
* $observation-category#activity "Atividade corporal"