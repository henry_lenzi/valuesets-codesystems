ValueSet: Parentesco
Id: br-core-parents-relationship
Description: "Classificação da relação familiar entre indivíduos."
* ^meta.lastUpdated = "2020-03-12T13:23:17.366+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRParentesco-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:23:16.6535149+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRParentesco|*