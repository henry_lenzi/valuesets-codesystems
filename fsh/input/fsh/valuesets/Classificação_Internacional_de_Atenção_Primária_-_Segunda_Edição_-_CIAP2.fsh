ValueSet: Classificação_Internacional_de_Atenção_Primária_-_Segunda_Edição_-_CIAP2
Id: br-core-condition-code-ciap2
Description: "Classifica os problemas identificados no contato assistencial pelos profissionais de saúde, os motivos da contato assistencial e as respostas propostas pela equipe seguindo a sistematização SOA."
* ^name = "Classificação Internacional de Atenção Primária - Segunda Edição - CIAP2"
* ^meta.lastUpdated = "2020-03-11T18:43:23.180+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRCIAP2-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-11T18:43:43.0294815+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRCIAP2|*