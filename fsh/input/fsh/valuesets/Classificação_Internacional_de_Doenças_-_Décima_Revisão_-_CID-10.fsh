ValueSet: Classificação_Internacional_de_Doenças_-_Décima_Revisão_-_CID-10
Id: 6dfb0714-7ff9-4a9f-9201-33943db7498c
Description: "Classificação Internacional de Doenças - Décima Revisão - CID-10"
* ^name = "Classificação Internacional de Doenças - Décima Revisão - CID-10"
* ^meta.lastUpdated = "2020-03-11T19:14:51.806+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRCID10-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-11T19:15:12.2909517+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRCID10|*