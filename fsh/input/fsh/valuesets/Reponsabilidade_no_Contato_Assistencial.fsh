ValueSet: Reponsabilidade_no_Contato_Assistencial
Id: br-core-encounter-participant-type
Description: "Classifica o tipo de responsabilidade de indivíduos ou profissionais no Contato Assistencial."
* ^name = "Reponsabilidade no Contato Assistencial"
* ^meta.lastUpdated = "2020-03-12T13:25:53.970+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRResponsabilidadeParticipante-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:25:53.2469711+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRResponsabilidadeParticipante|*