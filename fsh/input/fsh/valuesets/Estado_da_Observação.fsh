ValueSet: Estado_da_Observação
Id: 62f549ad-b1e4-4ff4-8e1c-abea77c5a2a6
Description: "Tipos de estados de uma observação."
* ^name = "Estado da Observação"
* ^meta.lastUpdated = "2020-06-14T01:24:41.508+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BREstadoObservacao-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-06-14T01:24:38.8931093+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* ObservationStatus#final "Concluída"
* ObservationStatus#entered-in-error "Cancelada por informação errada"