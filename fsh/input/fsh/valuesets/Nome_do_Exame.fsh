ValueSet: Nome_do_Exame
Id: br-core-observation-code
Description: "Classifica o exame laboratorial realizado."
* ^name = "Nome do Exame"
* ^meta.lastUpdated = "2020-03-26T13:20:24.501+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRNomeExame-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-26T13:20:23.3537413+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRNomeExameLOINC|*
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRNomeExameGAL|*