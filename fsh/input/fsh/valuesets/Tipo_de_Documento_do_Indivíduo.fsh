ValueSet: Tipo_de_Documento_do_Indivíduo
Id: br-core-identifier
Description: "Classifica o tipo de documento que está sendo utilizado para identificar o indivíduo."
* ^name = "Tipo de Documento do Indivíduo"
* ^meta.lastUpdated = "2020-03-12T13:27:33.397+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRTipoDocumentoIndividuo-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:27:32.6850777+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* $identifier-type#HC "Número do Cartão Nacional de Saúde (CNS)"
* $identifier-type#RRI "Número do Registro Geral (RG)"
* $identifier-type#MCT "Número da Certidão de Casamento"
* $identifier-type#PPN "Número do Passaporte"
* $identifier-type#RN "Número de inscrição no Conselho Regional de Enfermagem (COREn)"
* $identifier-type#SS "Número de Identificação Social (NIS/PIS/PASEP)"
* $identifier-type#DL "Número da Carteira Nacional de Habilitação (CNH)"
* $identifier-type#BCT "Número do Documento de Nascido Vivo (DN/DNV)"
* $identifier-type#BR "Número da Certidão de Nascimento"
* $identifier-type#IND "Número da Certidão de Índio"
* $identifier-type#MI "Número do Certificado de Reservista"
* $identifier-type#MD "Número de inscrição no Conselho Regional de Medicina (CRM)"
* $identifier-type#DDS "Número de inscrição no Conselho Regional Odontologia (CRO)"
* $identifier-type#TAX "Número de inscrição no Cadastro de Pessoas Físicas (CPF)"
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRTipoIdentificador|*
* exclude $BRTipoIdentificador#AUTH