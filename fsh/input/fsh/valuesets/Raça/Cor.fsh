ValueSet: Raça/Cor
Id: valueset-br-core-patient-raceethnicity-race
Description: "Códigos para representação de raça/cor no Brasil."
* ^meta.lastUpdated = "2020-03-12T13:25:39.619+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRRacaCor-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:25:38.9038219+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system $BRRacaCor