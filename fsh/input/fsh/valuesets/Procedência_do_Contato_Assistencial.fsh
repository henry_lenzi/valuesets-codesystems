ValueSet: Procedência_do_Contato_Assistencial
Id: br-core-encounter-hospitalization-admitsource
Description: "Classifica o serviço que encaminhou o indivíduo ou a sua iniciativa/de seu responsável na busca pelo acesso ao serviço de saúde."
* ^name = "Procedência do Contato Assistencial"
* ^meta.lastUpdated = "2020-03-12T13:23:45.969+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRProcedencia-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:23:45.1685886+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRProcedencia|*