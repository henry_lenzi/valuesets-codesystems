ValueSet: Posição_do_Indivíduo
Id: br-core-observation-posicaoindividuo
Description: "ValueSet utilizado para identificar a posição do indivíduo no momento da ação / procedimento."
* ^name = "Posição do Indivíduo"
* ^meta.lastUpdated = "2020-03-12T13:23:35.881+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/ValueSet/BRPosicaoIndividuo-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-12T13:23:34.4184256+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^immutable = false
* include codes from system http://www.saude.gov.br/fhir/r4/CodeSystem/BRPosicaoIndividuo|*