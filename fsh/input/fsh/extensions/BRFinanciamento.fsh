Extension: BRFinanciamento
Id: 3d75211e-ccb6-4e01-b965-05382abfae5d
Title: "Financiamento"
Description: "Extensão utilizada para identificar financiamento."
* ^meta.lastUpdated = "2020-04-07T17:02:20.726+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRFinanciamento-1.0"
* ^version = "1.0"
* ^date = "2020-04-07T17:02:17.5770638+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^context.type = #element
* ^context.expression = "Encounter.diagnosis.condition"
* . N
* . ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* . ^extension[=].valueCode = #4.0.0
* . ^short = "Financiamento"
* url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRFinanciamento-1.0" (exactly)
* value[x] 1.. N
* value[x] only CodeableConcept
* value[x] from $BRFinanciamento-1.0 (required)
* value[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* value[x] ^extension[=].valueCode = #4.0.0
* value[x] ^binding.description = "Financiamento"
* value[x].coding 1..1 N
* value[x].coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* value[x].coding ^extension[=].valueCode = #4.0.0
* value[x].coding.system 1.. N
* value[x].coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* value[x].coding.system ^extension[=].valueCode = #4.0.0
* value[x].coding.code 1.. N
* value[x].coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* value[x].coding.code ^extension[=].valueCode = #4.0.0
* value[x].coding.display ..0 N
* value[x].coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* value[x].coding.display ^extension[=].valueCode = #4.0.0
* value[x].coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* value[x].coding.display ^extension[=].valueBoolean = true
* value[x].coding.userSelected ..0 N
* value[x].coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* value[x].coding.userSelected ^extension[=].valueCode = #4.0.0
* value[x].text ..0 N
* value[x].text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* value[x].text ^extension[=].valueCode = #4.0.0
* value[x].text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* value[x].text ^extension[=].valueBoolean = true