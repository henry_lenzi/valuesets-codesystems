Extension: BRPosicaoIndividuo
Id: c966a277-85f3-4fc7-bfa7-314a94190df6
Title: "Posição do Indivíduo"
Description: "Extensão para indicar a posição do indivíduo."
* ^meta.lastUpdated = "2020-06-09T18:40:31.619+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRPosicaoIndividuo-1.0"
* ^version = "1.0"
* ^experimental = false
* ^date = "2020-06-09T18:40:55.1287932+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^context.type = #fhirpath
* ^context.expression = "BRObservacaoMensurada"
* . ..1
* . ^short = "Posição do Indivíduo"
* . ^definition = "Posição do indivíduo no momento da ação/procedimento."
* url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRPosicaoIndividuo-1.0" (exactly)
* value[x] 1..
* value[x] only CodeableConcept
* value[x] from $BRPosicaoIndividuo-1.0 (required)
* value[x] ^binding.description = "Posição do Indivíduo"
* value[x].coding 1..1
* value[x].coding.system 1..
* value[x].coding.code 1..
* value[x].coding.display ..0
* value[x].coding.display ^extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* value[x].coding.display ^extension.valueBoolean = true
* value[x].coding.userSelected ..0
* value[x].text ..0
* value[x].text ^extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* value[x].text ^extension.valueBoolean = true