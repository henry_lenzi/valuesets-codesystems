Profile: BRObservacaoDescritiva
Parent: Observation
Id: f0d41273-772c-425f-bb60-2507efddff01
Title: "Observação Descritiva"
Description: "Descrições textuais simples sobre um paciente."
* ^meta.lastUpdated = "2020-06-09T17:52:28.104+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRObservacaoDescritiva-1.0"
* ^version = "1.0"
* ^date = "2020-06-09T17:52:53.0098607+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* . MS
* identifier ..0
* basedOn ..0
* partOf ..0
* status MS
* status from $BREstadoObservacao-1.0 (required)
* status ^extension.url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-display-hint"
* status ^extension.valueString = "default: final"
* status ^short = "Estado da Observação"
* status ^definition = "Indica o estado que o contato assistencial se encontra quando é informado para a RNDS."
* status ^binding.description = "Estado da Observação"
* category 1..1
* category from $BRCategoriaObservacao-1.0 (preferred)
* category ^short = "Categoria da Observação"
* category ^definition = "Categoriza a observação que está sendo representada."
* category ^binding.extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName"
* category ^binding.extension.valueString = "ObservationCategory"
* category ^binding.description = "Categoria da Observação"
* category.coding 1..1
* category.coding.system 1..
* category.coding.code 1..
* category.coding.display ..0
* category.coding.display ^extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* category.coding.display ^extension.valueBoolean = true
* category.coding.userSelected ..0
* category.text ..0
* category.text ^extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* category.text ^extension.valueBoolean = true
* code MS
* code from $BRTipoObservacao-1.0 (required)
* code ^short = "Tipo da Observação"
* code ^definition = "Classifica o tipo de observação que está sendo representada."
* code ^binding.description = "Tipo de Observação"
* code.coding 1..1
* code.coding.system 1..
* code.coding.code 1..
* code.coding.display ..0
* code.coding.display ^extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* code.coding.display ^extension.valueBoolean = true
* code.coding.userSelected ..0
* code.text ..0
* code.text ^extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* code.text ^extension.valueBoolean = true
* subject 1.. MS
* subject only Reference(BRIndividuo)
* subject ^short = "Sujeito da Observação"
* subject ^definition = "Quem ou o quê a composição se refere. Pode ser um indivíduo, dispositivo, grupos (de indivíduos, dispositivos etc.)."
* subject.reference ..0
* subject.type ..0
* subject.type = "Patient" (exactly)
* subject.identifier 1..
* subject.identifier.use ..0
* subject.identifier.type ..0
* subject.identifier.system 1..
* subject.identifier.value 1..
* subject.identifier.period ..0
* subject.identifier.assigner ..0
* subject.display ..0
* subject.display ^extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* subject.display ^extension.valueBoolean = true
* focus ..0
* encounter ..0
* effective[x] ..0
* issued 1.. MS
* issued ^short = "Data/hora da Observação"
* issued ^definition = "Data ou data e hora que a Observação foi escrita."
* performer ..0
* value[x] 1.. MS
* value[x] only string
* value[x] ^short = "Resultado da Observação"
* value[x] ^definition = "Descrição textual da observação realizada."
* value[x] ^alias[0] = "Declaração subjetiva do indivíduo para o atendimento"
* value[x] ^alias[+] = "Resumo da evolução clínica do indivíduo durante a internação"
* value[x] ^alias[+] = "Dados do desfecho"
* value[x] ^alias[+] = "Informações adicionais/complementares"
* dataAbsentReason ..0
* interpretation ..0
* note ..0
* bodySite ..0
* method ..0
* specimen ..0
* device ..0
* referenceRange ..0
* hasMember ..0
* derivedFrom ..0
* component ..0