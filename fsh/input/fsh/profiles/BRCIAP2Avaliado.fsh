Profile: BRCIAP2Avaliado
Parent: Condition
Id: 8e81a097-e1cb-422f-8262-014d7ca329b2
Title: "CIAP2 Avaliado"
Description: "Problema atribuído pelo profissional de saúde ao indivíduo no contato assistencial."
* ^meta.lastUpdated = "2020-04-07T20:48:20.532+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRCIAP2Avaliado-1.0"
* ^version = "1.0"
* ^status = #active
* ^date = "2020-04-07T20:48:17.2731654+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* . MS
* . ^short = "Diagnóstico ou Problema"
* . ^definition = "Informações sobre a condição de saúde, lesão, deficiência ou qualquer outra questão que afete o bem-estar físico, mental ou social de um indivíduo identificadas em um contato assistencial. (adaptado do conceito de Problema/Diagnóstico do Clinical Knowledge Manager/OpenEHR)"
* identifier ..0 N
* identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier ^extension[=].valueCode = #4.0.0
* clinicalStatus MS N
* clinicalStatus from $BREstaadoResolucaoDiagnosticoProblema-1.0 (required)
* clinicalStatus ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* clinicalStatus ^extension[=].valueCode = #4.0.0
* clinicalStatus ^short = "Estado da Resolução"
* clinicalStatus ^definition = "Situação que o Diagnóstico/Problema se encontra no momento da informação.\r\nresolved: Resolvido\r\nactive: Resolvendo\r\nactive: Não resolvido\r\nnull: Indeterminado"
* clinicalStatus ^binding.description = "Estado da Resolução do Diagnóstico/Problema"
* clinicalStatus.coding 1..1 N
* clinicalStatus.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* clinicalStatus.coding ^extension[=].valueCode = #4.0.0
* clinicalStatus.coding.system 1.. N
* clinicalStatus.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* clinicalStatus.coding.system ^extension[=].valueCode = #4.0.0
* clinicalStatus.coding.code 1.. N
* clinicalStatus.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* clinicalStatus.coding.code ^extension[=].valueCode = #4.0.0
* clinicalStatus.coding.display ..0 N
* clinicalStatus.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* clinicalStatus.coding.display ^extension[=].valueCode = #4.0.0
* clinicalStatus.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* clinicalStatus.coding.display ^extension[=].valueBoolean = true
* clinicalStatus.coding.userSelected ..0 N
* clinicalStatus.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* clinicalStatus.coding.userSelected ^extension[=].valueCode = #4.0.0
* clinicalStatus.text ..0 N
* clinicalStatus.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* clinicalStatus.text ^extension[=].valueCode = #4.0.0
* clinicalStatus.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* clinicalStatus.text ^extension[=].valueBoolean = true
* verificationStatus ..0 N
* verificationStatus ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* verificationStatus ^extension[=].valueCode = #4.0.0
* category ..0 N
* category ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category ^extension[=].valueCode = #4.0.0
* severity ..0 N
* severity ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* severity ^extension[=].valueCode = #4.0.0
* code 1.. MS N
* code from $BRCIAP2-1.0 (required)
* code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code ^extension[=].valueCode = #4.0.0
* code ^short = "Diagnóstico ou Problema"
* code ^definition = "Diagnóstico ou problema avaliado no indivíduo durante o contato assistencial."
* code ^binding.description = "CIAP2"
* code.coding 1..1 N
* code.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding ^extension[=].valueCode = #4.0.0
* code.coding.system 1.. N
* code.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.system ^extension[=].valueCode = #4.0.0
* code.coding.code 1.. N
* code.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.code ^extension[=].valueCode = #4.0.0
* code.coding.display ..0 N
* code.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.display ^extension[=].valueCode = #4.0.0
* code.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* code.coding.display ^extension[=].valueBoolean = true
* code.coding.userSelected ..0 N
* code.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.userSelected ^extension[=].valueCode = #4.0.0
* code.text ..0 N
* code.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.text ^extension[=].valueCode = #4.0.0
* code.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* code.text ^extension[=].valueBoolean = true
* bodySite ..0 N
* bodySite ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* bodySite ^extension[=].valueCode = #4.0.0
* subject only Reference(BRIndividuo)
* subject MS N
* subject ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject ^extension[=].valueCode = #4.0.0
* subject ^short = "Indivíduo"
* subject ^definition = "Indica o indivíduo ao qual o CIAP2 se refere."
* subject.extension ^slicing.discriminator.type = #value
* subject.extension ^slicing.discriminator.path = "url"
* subject.extension ^slicing.rules = #open
* subject.extension[unidentifiedPatient] only BRIndividuoNaoIdentificado
* subject.extension[unidentifiedPatient] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient] ^sliceName = "unidentifiedPatient"
* subject.extension[unidentifiedPatient] ^short = "Dados do Indivíduo Não Identificado"
* subject.extension[unidentifiedPatient] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[gender] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[gender] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[gender] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[gender] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[gender] ^sliceName = "gender"
* subject.extension[unidentifiedPatient].extension[gender] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[birthYear] ^sliceName = "birthYear"
* subject.extension[unidentifiedPatient].extension[birthYear] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[reason] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[reason] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[reason] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[reason] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[reason] ^sliceName = "reason"
* subject.extension[unidentifiedPatient].extension[reason] ^mustSupport = true
* subject.reference ..0 N
* subject.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.reference ^extension[=].valueCode = #4.0.0
* subject.type ..0 N
* subject.type = "Patient" (exactly)
* subject.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.type ^extension[=].valueCode = #4.0.0
* subject.identifier 1.. N
* subject.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier ^extension[=].valueCode = #4.0.0
* subject.identifier.use ..0 N
* subject.identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.use ^extension[=].valueCode = #4.0.0
* subject.identifier.type ..0 N
* subject.identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.type ^extension[=].valueCode = #4.0.0
* subject.identifier.system 1.. N
* subject.identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.system ^extension[=].valueCode = #4.0.0
* subject.identifier.value 1.. N
* subject.identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.value ^extension[=].valueCode = #4.0.0
* subject.identifier.period ..0 N
* subject.identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.period ^extension[=].valueCode = #4.0.0
* subject.identifier.assigner ..0 N
* subject.identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.assigner ^extension[=].valueCode = #4.0.0
* subject.display ..0 N
* subject.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.display ^extension[=].valueCode = #4.0.0
* subject.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* subject.display ^extension[=].valueBoolean = true
* encounter ..0 N
* encounter ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* encounter ^extension[=].valueCode = #4.0.0
* onset[x] ..0 N
* onset[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* onset[x] ^extension[=].valueCode = #4.0.0
* abatement[x] ..0 N
* abatement[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* abatement[x] ^extension[=].valueCode = #4.0.0
* recordedDate ..0 N
* recordedDate ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* recordedDate ^extension[=].valueCode = #4.0.0
* recorder ..0 N
* recorder ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* recorder ^extension[=].valueCode = #4.0.0
* asserter ..0 N
* asserter ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* asserter ^extension[=].valueCode = #4.0.0
* stage ..0 N
* stage ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* stage ^extension[=].valueCode = #4.0.0
* evidence ..0 N
* evidence ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* evidence ^extension[=].valueCode = #4.0.0
* note ..1 MS N
* note ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note ^extension[=].valueCode = #4.0.0
* note.author[x] ..0 N
* note.author[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note.author[x] ^extension[=].valueCode = #4.0.0
* note.time ..0 N
* note.time ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note.time ^extension[=].valueCode = #4.0.0
* note.text MS N
* note.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note.text ^extension[=].valueCode = #4.0.0
* note.text ^short = "Nota"
* note.text ^definition = "Informações complementares textuais acerca do CIAP2 avaliado."