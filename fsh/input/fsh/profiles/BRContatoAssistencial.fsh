Profile: BRContatoAssistencial
Parent: Encounter
Id: 9e02be1a-ccd7-41d3-a10e-2c031a4979b4
Title: "Contato Assistencial"
Description: """Resumo ou sumário referente a um atendimento ininterrupto dispensado a um indivíduo em uma mesma modalidade assistencial e em um mesmo estabelecimento de saúde, gerado após a conclusão deste atendimento.
Referência: <a href="https://wiki.saude.gov.br/cmd/index.php/Página_principal#Contato_Assistencial">https://wiki.saude.gov.br/cmd/index.php/Página_principal#Contato_Assistencial</a>"""
* ^meta.lastUpdated = "2020-04-29T19:58:11.640+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRContatoAssistencial-1.0"
* ^version = "1.0"
* ^status = #active
* ^experimental = false
* ^date = "2020-04-29T19:58:09.2865128+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^purpose = "Reportar para o Sistema Único de Saúde dados mínimos referentes a um atendimento concluído, de forma a possibilitar seu compartilhamento entre profissionais de saúde, o próprio indivíduo que recebeu o atendimento, assim como possibilitar o uso secundário das informações para atividades faturamento, auditoria, formulação e monitoramento de políticas públicas, por exemplo, nas esferas municipal, estadual e federal."
* . MS
* . ^short = "Contato Assistencial"
* . ^definition = "Atenção à saúde dispensada a um indivíduo em uma modalidade assistencial, de forma ininterrupta e em um mesmo estabelecimento de saúde."
* . ^alias[0] = "Atendimento"
* . ^alias[+] = "Visita"
* . ^alias[+] = "Internação"
* . ^alias[+] = "Consulta"
* . ^alias[+] = "Atendimento Ambulatorial"
* identifier ..0 N
* identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier ^extension[=].valueCode = #4.0.0
* status MS N
* status from $BREstadoContatoAssistencial-1.0 (required)
* status ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* status ^extension[=].valueCode = #4.0.0
* status ^short = "finished | entered-in-error"
* status ^definition = "Indica o estado que o contato assistencial se encontra quando é informado para a RNDS."
* status ^comment = "Na RNDS os contatos assistenciais somente são informados após terem sido finalizados, portanto somente a opção finished pode ser utilizada, ou entered-in-error, quando o contato assistencial foi enviado com erro."
* status ^binding.description = "Estado do Contato Assistencial"
* statusHistory ..0 N
* statusHistory ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* statusHistory ^extension[=].valueCode = #4.0.0
* statusHistory ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-explicit-type-name"
* statusHistory ^extension[=].valueString = "StatusHistory"
* class MS N
* class from $BRModalidadeAssistencial-1.0 (required)
* class ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* class ^extension[=].valueCode = #4.0.0
* class ^label = "Modalidade Assistencial"
* class ^short = "Modalidade Assistencial"
* class ^definition = "Classifica os contatos assistenciais de acordo com as especificidades do modo, local e duração do atendimento.\r\nReferência: https://rts.saude.gov.br/#/modalidade-assistencial"
* class ^binding.description = "Modalidade Assistencial"
* class.system 1.. N
* class.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* class.system ^extension[=].valueCode = #4.0.0
* class.code 1.. N
* class.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* class.code ^extension[=].valueCode = #4.0.0
* class.display ..0 N
* class.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* class.display ^extension[=].valueCode = #4.0.0
* class.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* class.display ^extension[=].valueBoolean = true
* class.userSelected ..0 N
* class.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* class.userSelected ^extension[=].valueCode = #4.0.0
* classHistory ..0 N
* classHistory ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* classHistory ^extension[=].valueCode = #4.0.0
* classHistory ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-explicit-type-name"
* classHistory ^extension[=].valueString = "ClassHistory"
* type ..0 N
* type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* type ^extension[=].valueCode = #4.0.0
* serviceType ..0 N
* serviceType ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceType ^extension[=].valueCode = #4.0.0
* priority 1.. MS N
* priority from $BRCaraterAtendimento-1.0 (required)
* priority ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* priority ^extension[=].valueCode = #4.0.0
* priority ^label = "Caráter do Atendimento"
* priority ^short = "Caráter do Atendimento"
* priority ^definition = "Identifica o contato assistencial de acordo com a prioridade de sua realização.\r\nReferência: https://rts.saude.gov.br/#/carater-atendimento"
* priority ^alias[0] = "Caráter de Atendimento"
* priority ^alias[+] = "Caráter da Internação"
* priority ^binding.description = "Identifica o contato assistencial de acordo com a prioridade de sua realização."
* priority.coding 1..1 N
* priority.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* priority.coding ^extension[=].valueCode = #4.0.0
* priority.coding.system 1.. N
* priority.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* priority.coding.system ^extension[=].valueCode = #4.0.0
* priority.coding.code 1.. N
* priority.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* priority.coding.code ^extension[=].valueCode = #4.0.0
* priority.coding.display ..0 N
* priority.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* priority.coding.display ^extension[=].valueCode = #4.0.0
* priority.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* priority.coding.display ^extension[=].valueBoolean = true
* priority.coding.userSelected ..0 N
* priority.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* priority.coding.userSelected ^extension[=].valueCode = #4.0.0
* priority.text ..0 N
* priority.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* priority.text ^extension[=].valueCode = #4.0.0
* priority.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* priority.text ^extension[=].valueBoolean = true
* subject 1.. MS N
* subject only Reference(BRIndividuo)
* subject ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject ^extension[=].valueCode = #4.0.0
* subject ^comment = "Na primeira versão não será suportado a identificação de grupos."
* subject.extension ^slicing.discriminator.type = #value
* subject.extension ^slicing.discriminator.path = "url"
* subject.extension ^slicing.rules = #open
* subject.extension[unidentifiedPatient] only BRIndividuoNaoIdentificado
* subject.extension[unidentifiedPatient] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient] ^sliceName = "unidentifiedPatient"
* subject.extension[unidentifiedPatient] ^short = "Dados do Indivíduo Não Identificado"
* subject.extension[unidentifiedPatient] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[gender] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[gender] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[gender] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[gender] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[gender] ^sliceName = "gender"
* subject.extension[unidentifiedPatient].extension[gender] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[birthYear] ^sliceName = "birthYear"
* subject.extension[unidentifiedPatient].extension[birthYear] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[reason] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[reason] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[reason] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[reason] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[reason] ^sliceName = "reason"
* subject.extension[unidentifiedPatient].extension[reason] ^mustSupport = true
* subject.reference ..0 N
* subject.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.reference ^extension[=].valueCode = #4.0.0
* subject.type ..0 N
* subject.type = "Patient" (exactly)
* subject.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.type ^extension[=].valueCode = #4.0.0
* subject.identifier.use ..0 N
* subject.identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.use ^extension[=].valueCode = #4.0.0
* subject.identifier.type ..0 N
* subject.identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.type ^extension[=].valueCode = #4.0.0
* subject.identifier.system 1.. N
* subject.identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.system ^extension[=].valueCode = #4.0.0
* subject.identifier.value 1.. N
* subject.identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.value ^extension[=].valueCode = #4.0.0
* subject.identifier.period ..0 N
* subject.identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.period ^extension[=].valueCode = #4.0.0
* subject.identifier.assigner ..0 N
* subject.identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.assigner ^extension[=].valueCode = #4.0.0
* subject.display ..0 N
* subject.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.display ^extension[=].valueCode = #4.0.0
* subject.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* subject.display ^extension[=].valueBoolean = true
* subject.display ^short = "Nome ou descrição do sujeito"
* subject.display ^definition = "Nome do indivíduo ou descrição do sujeito do Contato Assistencial quando não for um indivíduo."
* episodeOfCare ..0 N
* episodeOfCare ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* episodeOfCare ^extension[=].valueCode = #4.0.0
* basedOn ..0 N
* basedOn ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* basedOn ^extension[=].valueCode = #4.0.0
* participant MS N
* participant ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant ^extension[=].valueCode = #4.0.0
* participant ^short = "Participante(s) do Contato Assistencial"
* participant ^definition = "Identifica o(s) profissional(is) ou indivíduo(s) que teve(tiveram) algum tipo de participação no Contato Assistencial."
* participant ^alias[0] = "Profissionais do Atendimento"
* participant ^alias[+] = "Profissional Responsável pela Alta"
* participant.extension ^slicing.discriminator.type = #value
* participant.extension ^slicing.discriminator.path = "url"
* participant.extension ^slicing.rules = #open
* participant.extension contains
    BROcupacao named function 1..1 MS N and
    BRIdentificacaoEquipe named team 0..1 MS N
* participant.extension[function] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.extension[function] ^extension[=].valueCode = #4.0.0
* participant.extension[function] ^definition = "Ocupação do profissional ou indivíduo que participou do contato assistencial."
* participant.extension[team] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.extension[team] ^extension[=].valueCode = #4.0.0
* participant.extension[team] ^short = "Equipe"
* participant.extension[team] ^definition = "Identifica a equipe de saúde responsável pelo Contato Assistencial utilizando o Identificador Nacional de Equipes."
* participant.type 1..1 MS N
* participant.type from $BRResponsabilidadeParticipante-1.0 (required)
* participant.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.type ^extension[=].valueCode = #4.0.0
* participant.type ^short = "Tipo de Participação"
* participant.type ^definition = "Responsabilidade que o profissional ou indivíduo teve durante o Contato Assistencial."
* participant.type ^binding.description = "Responsabilidade no Contato Assistencial"
* participant.type.coding.system 1.. N
* participant.type.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.type.coding.system ^extension[=].valueCode = #4.0.0
* participant.type.coding.code 1.. N
* participant.type.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.type.coding.code ^extension[=].valueCode = #4.0.0
* participant.type.coding.display ..0 N
* participant.type.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.type.coding.display ^extension[=].valueCode = #4.0.0
* participant.type.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* participant.type.coding.display ^extension[=].valueBoolean = true
* participant.type.coding.userSelected ..0 N
* participant.type.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.type.coding.userSelected ^extension[=].valueCode = #4.0.0
* participant.type.text ..0 N
* participant.type.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.type.text ^extension[=].valueCode = #4.0.0
* participant.type.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* participant.type.text ^extension[=].valueBoolean = true
* participant.period ..0 N
* participant.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.period ^extension[=].valueCode = #4.0.0
* participant.individual 1.. MS N
* participant.individual only Reference(BRLotacaoProfissional)
* participant.individual ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual ^extension[=].valueCode = #4.0.0
* participant.individual ^short = "Participante"
* participant.individual ^definition = "Identifica o profissional ou indivíduo teve alguma participação no Contato Assistencial."
* participant.individual.reference ..0 N
* participant.individual.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.reference ^extension[=].valueCode = #4.0.0
* participant.individual.type ..0 N
* participant.individual.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.type ^extension[=].valueCode = #4.0.0
* participant.individual.identifier 1.. N
* participant.individual.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.identifier ^extension[=].valueCode = #4.0.0
* participant.individual.identifier.use ..0 N
* participant.individual.identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.identifier.use ^extension[=].valueCode = #4.0.0
* participant.individual.identifier.type ..0 N
* participant.individual.identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.identifier.type ^extension[=].valueCode = #4.0.0
* participant.individual.identifier.system 1.. N
* participant.individual.identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.identifier.system ^extension[=].valueCode = #4.0.0
* participant.individual.identifier.value 1.. N
* participant.individual.identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.identifier.value ^extension[=].valueCode = #4.0.0
* participant.individual.identifier.period ..0 N
* participant.individual.identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.identifier.period ^extension[=].valueCode = #4.0.0
* participant.individual.identifier.assigner ..0 N
* participant.individual.identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.identifier.assigner ^extension[=].valueCode = #4.0.0
* participant.individual.display ..0 N
* participant.individual.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* participant.individual.display ^extension[=].valueCode = #4.0.0
* participant.individual.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* participant.individual.display ^extension[=].valueBoolean = true
* appointment ..0 N
* appointment ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* appointment ^extension[=].valueCode = #4.0.0
* period 1.. MS N
* period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* period ^extension[=].valueCode = #4.0.0
* period ^short = "Início e Término do Contato Assistencial"
* period ^definition = "Data ou data/hora de adminissão e desfecho do Contato Assistencial."
* period.start 1.. MS N
* period.start ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* period.start ^extension[=].valueCode = #4.0.0
* period.start ^short = "Momento da Admissão"
* period.start ^definition = "Competência (mês/ano), data ou data e hora do início do Contato Assistencial."
* period.start ^alias[0] = "Data da Admissão"
* period.start ^alias[+] = "Data e Hora do Atendimento"
* period.start ^alias[+] = "Data e Hora da Admissão"
* period.end 1.. MS N
* period.end ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* period.end ^extension[=].valueCode = #4.0.0
* period.end ^short = "Momento do Desfecho"
* period.end ^definition = "Competência (mês/ano), data ou data e hora do término do Contato Assistencial."
* period.end ^alias[0] = "Data do Desfecho"
* period.end ^alias[+] = "Data e Hora da Saída da Internação"
* length ..0 N
* length ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* length ^extension[=].valueCode = #4.0.0
* reasonCode ..0 N
* reasonCode ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonCode ^extension[=].valueCode = #4.0.0
* reasonReference only Reference(BRCID10Avaliado or BRCIAP2Avaliado or BRObservacaoDescritiva)
* reasonReference MS N
* reasonReference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference ^extension[=].valueCode = #4.0.0
* reasonReference ^slicing.discriminator.type = #profile
* reasonReference ^slicing.discriminator.path = "resolve()"
* reasonReference ^slicing.rules = #open
* reasonReference ^short = "Motivo do Atendimento"
* reasonReference ^definition = "Motivação para o Contato Assistencial acontecer."
* reasonReference ^alias[0] = "Motivo do Contato Assistencial"
* reasonReference ^alias[+] = "Diangóstico Principal"
* reasonReference ^alias[+] = "Motivo da Admissão"
* reasonReference contains
    primaryDiagnosis 0..1 MS N and
    reasonCoded 0..* MS N and
    reasonText 0..1 MS N
* reasonReference[primaryDiagnosis] only Reference(BRCID10Avaliado)
* reasonReference[primaryDiagnosis] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[primaryDiagnosis] ^extension[=].valueCode = #4.0.0
* reasonReference[primaryDiagnosis] ^short = "Diagnóstico Principal"
* reasonReference[primaryDiagnosis] ^definition = "Condição estabelecida após estudo de forma a esclarecer qual o mais importante ou principal motivo responsável pela demanda do contato assistencial. O diagnóstico principal reflete achados clínicos descobertos durante a permanência do indivíduo no estabelecimento de saúde, podendo portanto ser diferente do diagnóstico de admissão. (Port. nº 1.324/SAS/MS/2014)."
* reasonReference[primaryDiagnosis].extension ^slicing.discriminator.type = #value
* reasonReference[primaryDiagnosis].extension ^slicing.discriminator.path = "url"
* reasonReference[primaryDiagnosis].extension ^slicing.rules = #open
* reasonReference[primaryDiagnosis].extension[admissionStatus] only BRIndicadorPresencaAdmissao
* reasonReference[primaryDiagnosis].extension[admissionStatus] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* reasonReference[primaryDiagnosis].extension[admissionStatus] ^extension[=].valueCode = #normative
* reasonReference[primaryDiagnosis].extension[admissionStatus] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[primaryDiagnosis].extension[admissionStatus] ^extension[=].valueCode = #4.0.0
* reasonReference[primaryDiagnosis].extension[admissionStatus] ^sliceName = "admissionStatus"
* reasonReference[primaryDiagnosis].extension[admissionStatus] ^definition = "Identifica se o problema/diagnóstico era previamente conhecido na admissão do indivíduo para o contato assistencial.\r\nUtilize true para Sim, false para Não e null para Desconhecido."
* reasonReference[primaryDiagnosis].extension[admissionStatus] ^mustSupport = true
* reasonReference[primaryDiagnosis].reference 1.. N
* reasonReference[primaryDiagnosis].reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[primaryDiagnosis].reference ^extension[=].valueCode = #4.0.0
* reasonReference[primaryDiagnosis].type ..0 N
* reasonReference[primaryDiagnosis].type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[primaryDiagnosis].type ^extension[=].valueCode = #4.0.0
* reasonReference[primaryDiagnosis].identifier ..0 N
* reasonReference[primaryDiagnosis].identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[primaryDiagnosis].identifier ^extension[=].valueCode = #4.0.0
* reasonReference[primaryDiagnosis].display ..0 N
* reasonReference[primaryDiagnosis].display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[primaryDiagnosis].display ^extension[=].valueCode = #4.0.0
* reasonReference[primaryDiagnosis].display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* reasonReference[primaryDiagnosis].display ^extension[=].valueBoolean = true
* reasonReference[reasonCoded] only Reference(BRCIAP2Avaliado)
* reasonReference[reasonCoded] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonCoded] ^extension[=].valueCode = #4.0.0
* reasonReference[reasonCoded] ^short = "Motivo do Atendimento Estruturado"
* reasonReference[reasonCoded] ^definition = "Motivo do Contato Assistencial ter acontecido."
* reasonReference[reasonCoded] ^alias[0] = "Motivo do Atendimento"
* reasonReference[reasonCoded].reference 1.. N
* reasonReference[reasonCoded].reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonCoded].reference ^extension[=].valueCode = #4.0.0
* reasonReference[reasonCoded].type ..0 N
* reasonReference[reasonCoded].type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonCoded].type ^extension[=].valueCode = #4.0.0
* reasonReference[reasonCoded].identifier ..0 N
* reasonReference[reasonCoded].identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonCoded].identifier ^extension[=].valueCode = #4.0.0
* reasonReference[reasonCoded].display ..0 N
* reasonReference[reasonCoded].display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonCoded].display ^extension[=].valueCode = #4.0.0
* reasonReference[reasonCoded].display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* reasonReference[reasonCoded].display ^extension[=].valueBoolean = true
* reasonReference[reasonText] only Reference(BRObservacaoDescritiva)
* reasonReference[reasonText] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonText] ^extension[=].valueCode = #4.0.0
* reasonReference[reasonText] ^short = "Declaração Subjetiva do Motivo do Atendimento"
* reasonReference[reasonText] ^definition = "Declaração subjetiva do indivíduo para indicar a razão do Contato Assistencial."
* reasonReference[reasonText] ^alias[0] = "Declaração Subjetiva do Indivíudo para o Atendimento"
* reasonReference[reasonText].reference 1.. N
* reasonReference[reasonText].reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonText].reference ^extension[=].valueCode = #4.0.0
* reasonReference[reasonText].type ..0 N
* reasonReference[reasonText].type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonText].type ^extension[=].valueCode = #4.0.0
* reasonReference[reasonText].identifier ..0 N
* reasonReference[reasonText].identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonText].identifier ^extension[=].valueCode = #4.0.0
* reasonReference[reasonText].display ..0 N
* reasonReference[reasonText].display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference[reasonText].display ^extension[=].valueCode = #4.0.0
* reasonReference[reasonText].display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* reasonReference[reasonText].display ^extension[=].valueBoolean = true
* diagnosis N
* diagnosis ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis ^extension[=].valueCode = #4.0.0
* diagnosis ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-explicit-type-name"
* diagnosis ^extension[=].valueString = "Diagnosis"
* diagnosis ^slicing.discriminator.type = #profile
* diagnosis ^slicing.discriminator.path = "condition.resolve()"
* diagnosis ^slicing.rules = #open
* diagnosis ^short = "Diagnósticos Secundários, Outros Problemas e Procedimentos"
* diagnosis ^definition = "Referência a diagnóstico(s) secundário(s) avaliado(s), outro(s) problema(s) avaliado(s) e/ou procedimento(s) realizado(s) durante o contato assistencial."
* diagnosis ^alias[0] = "Diagnóstico Secundário"
* diagnosis ^alias[+] = "Lista de Problemas"
* diagnosis ^alias[+] = "Problema Avaliado"
* diagnosis ^alias[+] = "Diagnóstico Avaliado"
* diagnosis ^alias[+] = "Procedimento Executado"
* diagnosis ^alias[+] = "Procedimento Realizado"
* diagnosis contains
    diagnosis 0..* MS N and
    problem 0..* MS N and
    procedure 1..* MS N
* diagnosis[diagnosis] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis] ^extension[=].valueCode = #4.0.0
* diagnosis[diagnosis] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-explicit-type-name"
* diagnosis[diagnosis] ^extension[=].valueString = "Diagnosis"
* diagnosis[diagnosis] ^short = "Diagnóstico(s) Avaliado(s)"
* diagnosis[diagnosis] ^definition = "Informações sobre a condição de saúde, lesão, deficiência ou qualquer outra questão que afete o bem-estar físico, mental ou social de um indivíduo identificadas em um contato assistencial. (adaptado do conceito de Problema/Diagnóstico do Clinical Knowledge Manager/OpenEHR)\r\nDevem ser listados diangnósticos avaliados durante um contato assistencial ou serem listados os diangósticos secundários caso o reasonReference:primaryDiagnosis seja informado; o diagnóstico informado no reasonReference não deve ser repetido neste elemento.\r\nDiagnóstico secundário é(são) a(s) condição(ões) que coexiste(m) no momento da admissão, que se desenvolve(m) durante o contato assistencial ou que afeta(m) a atenção recebida e/ou a duração do contato assistencial. (adaptado do conceito disposto no Art. 3º da Portaria SAS/MS nº 1.324, de 27 de novembro de 2014)"
* diagnosis[diagnosis] ^alias[0] = "Diagnóstico Secundário"
* diagnosis[diagnosis].condition only Reference(BRCID10Avaliado)
* diagnosis[diagnosis].condition MS N
* diagnosis[diagnosis].condition ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis].condition ^extension[=].valueCode = #4.0.0
* diagnosis[diagnosis].condition.extension ^slicing.discriminator.type = #value
* diagnosis[diagnosis].condition.extension ^slicing.discriminator.path = "url"
* diagnosis[diagnosis].condition.extension ^slicing.rules = #open
* diagnosis[diagnosis].condition.extension[admissionStatus] only BRIndicadorPresencaAdmissao
* diagnosis[diagnosis].condition.extension[admissionStatus] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* diagnosis[diagnosis].condition.extension[admissionStatus] ^extension[=].valueCode = #normative
* diagnosis[diagnosis].condition.extension[admissionStatus] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis].condition.extension[admissionStatus] ^extension[=].valueCode = #4.0.0
* diagnosis[diagnosis].condition.extension[admissionStatus] ^sliceName = "admissionStatus"
* diagnosis[diagnosis].condition.extension[admissionStatus] ^definition = "Identifica se o problema/diagnóstico era previamente conhecido na admissão do indivíduo para o contato assistencial.\r\nUtilize true para Sim, false para Não e null para Desconhecido."
* diagnosis[diagnosis].condition.extension[admissionStatus] ^mustSupport = true
* diagnosis[diagnosis].condition.reference 1.. N
* diagnosis[diagnosis].condition.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis].condition.reference ^extension[=].valueCode = #4.0.0
* diagnosis[diagnosis].condition.type ..0 N
* diagnosis[diagnosis].condition.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis].condition.type ^extension[=].valueCode = #4.0.0
* diagnosis[diagnosis].condition.identifier ..0 N
* diagnosis[diagnosis].condition.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis].condition.identifier ^extension[=].valueCode = #4.0.0
* diagnosis[diagnosis].condition.display ..0 N
* diagnosis[diagnosis].condition.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis].condition.display ^extension[=].valueCode = #4.0.0
* diagnosis[diagnosis].condition.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* diagnosis[diagnosis].condition.display ^extension[=].valueBoolean = true
* diagnosis[diagnosis].use ..0 N
* diagnosis[diagnosis].use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis].use ^extension[=].valueCode = #4.0.0
* diagnosis[diagnosis].rank ..0 N
* diagnosis[diagnosis].rank ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[diagnosis].rank ^extension[=].valueCode = #4.0.0
* diagnosis[problem] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[problem] ^extension[=].valueCode = #4.0.0
* diagnosis[problem] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-explicit-type-name"
* diagnosis[problem] ^extension[=].valueString = "Diagnosis"
* diagnosis[problem] ^short = "Problema(s) Avaliado(s)"
* diagnosis[problem] ^definition = "Informações sobre a condição de saúde, lesão, deficiência ou qualquer outra questão que afete o bem-estar físico, mental ou social de um indivíduo identificadas em um contato assistencial. (adaptado do conceito de Problema/Diagnóstico do Clinical Knowledge Manager/OpenEHR)\r\nO(s) problema(s) informado(s) no reasonReference:reasonCodede não deve(m) ser repetido(s) neste elemento."
* diagnosis[problem] ^alias[0] = "Lista de Problemas"
* diagnosis[problem].condition only Reference(BRCIAP2Avaliado)
* diagnosis[problem].condition MS N
* diagnosis[problem].condition ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[problem].condition ^extension[=].valueCode = #4.0.0
* diagnosis[problem].condition.reference 1.. N
* diagnosis[problem].condition.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[problem].condition.reference ^extension[=].valueCode = #4.0.0
* diagnosis[problem].condition.type ..0 N
* diagnosis[problem].condition.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[problem].condition.type ^extension[=].valueCode = #4.0.0
* diagnosis[problem].condition.identifier ..0 N
* diagnosis[problem].condition.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[problem].condition.identifier ^extension[=].valueCode = #4.0.0
* diagnosis[problem].condition.display ..0 N
* diagnosis[problem].condition.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[problem].condition.display ^extension[=].valueCode = #4.0.0
* diagnosis[problem].condition.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* diagnosis[problem].condition.display ^extension[=].valueBoolean = true
* diagnosis[problem].use ..0 N
* diagnosis[problem].use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[problem].use ^extension[=].valueCode = #4.0.0
* diagnosis[problem].rank ..0 N
* diagnosis[problem].rank ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[problem].rank ^extension[=].valueCode = #4.0.0
* diagnosis[procedure] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure] ^extension[=].valueCode = #4.0.0
* diagnosis[procedure] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-explicit-type-name"
* diagnosis[procedure] ^extension[=].valueString = "Diagnosis"
* diagnosis[procedure] ^short = "Procedimento(s) Realizado(s)"
* diagnosis[procedure] ^definition = "Ação(ões) e/ou serviço(s) de saúde realizado(s) no/para o indivíduo."
* diagnosis[procedure] ^alias[0] = "Procedimento Executado"
* diagnosis[procedure].condition only Reference(BRProcedimentoRealizado)
* diagnosis[procedure].condition MS N
* diagnosis[procedure].condition ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure].condition ^extension[=].valueCode = #4.0.0
* diagnosis[procedure].condition.extension ^slicing.discriminator.type = #value
* diagnosis[procedure].condition.extension ^slicing.discriminator.path = "url"
* diagnosis[procedure].condition.extension ^slicing.rules = #open
* diagnosis[procedure].condition.extension contains BRFinanciamento named financier 1..1 MS N
* diagnosis[procedure].condition.extension[financier] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure].condition.extension[financier] ^extension[=].valueCode = #4.0.0
* diagnosis[procedure].condition.extension[financier] ^definition = "Agente, instituição ou entidade responsável por custear as ações e serviços de saúde."
* diagnosis[procedure].condition.reference 1.. N
* diagnosis[procedure].condition.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure].condition.reference ^extension[=].valueCode = #4.0.0
* diagnosis[procedure].condition.type ..0 N
* diagnosis[procedure].condition.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure].condition.type ^extension[=].valueCode = #4.0.0
* diagnosis[procedure].condition.identifier ..0 N
* diagnosis[procedure].condition.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure].condition.identifier ^extension[=].valueCode = #4.0.0
* diagnosis[procedure].condition.display ..0 N
* diagnosis[procedure].condition.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure].condition.display ^extension[=].valueCode = #4.0.0
* diagnosis[procedure].condition.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* diagnosis[procedure].condition.display ^extension[=].valueBoolean = true
* diagnosis[procedure].use ..0 N
* diagnosis[procedure].use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure].use ^extension[=].valueCode = #4.0.0
* diagnosis[procedure].rank ..0 N
* diagnosis[procedure].rank ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* diagnosis[procedure].rank ^extension[=].valueCode = #4.0.0
* account ..0 N
* account ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* account ^extension[=].valueCode = #4.0.0
* hospitalization 1.. MS N
* hospitalization ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization ^extension[=].valueCode = #4.0.0
* hospitalization.preAdmissionIdentifier ..0 N
* hospitalization.preAdmissionIdentifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.preAdmissionIdentifier ^extension[=].valueCode = #4.0.0
* hospitalization.origin ..0 N
* hospitalization.origin ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.origin ^extension[=].valueCode = #4.0.0
* hospitalization.admitSource 1.. MS N
* hospitalization.admitSource from $BRProcedencia-1.0 (required)
* hospitalization.admitSource ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.admitSource ^extension[=].valueCode = #4.0.0
* hospitalization.admitSource ^short = "Procedência"
* hospitalization.admitSource ^definition = "Identifica o serviço que encaminhou o indivíduo ou a sua iniciativa/de seu responsável na busca pelo acesso ao serviço de saúde."
* hospitalization.admitSource ^binding.description = "Procedência"
* hospitalization.admitSource.coding 1..1 N
* hospitalization.admitSource.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.admitSource.coding ^extension[=].valueCode = #4.0.0
* hospitalization.admitSource.coding.system 1.. N
* hospitalization.admitSource.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.admitSource.coding.system ^extension[=].valueCode = #4.0.0
* hospitalization.admitSource.coding.code 1.. N
* hospitalization.admitSource.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.admitSource.coding.code ^extension[=].valueCode = #4.0.0
* hospitalization.admitSource.coding.display ..0 N
* hospitalization.admitSource.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.admitSource.coding.display ^extension[=].valueCode = #4.0.0
* hospitalization.admitSource.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* hospitalization.admitSource.coding.display ^extension[=].valueBoolean = true
* hospitalization.admitSource.coding.userSelected ..0 N
* hospitalization.admitSource.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.admitSource.coding.userSelected ^extension[=].valueCode = #4.0.0
* hospitalization.admitSource.text ..0 N
* hospitalization.admitSource.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.admitSource.text ^extension[=].valueCode = #4.0.0
* hospitalization.admitSource.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* hospitalization.admitSource.text ^extension[=].valueBoolean = true
* hospitalization.reAdmission ..0 N
* hospitalization.reAdmission ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.reAdmission ^extension[=].valueCode = #4.0.0
* hospitalization.dietPreference ..0 N
* hospitalization.dietPreference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.dietPreference ^extension[=].valueCode = #4.0.0
* hospitalization.specialCourtesy ..0 N
* hospitalization.specialCourtesy ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.specialCourtesy ^extension[=].valueCode = #4.0.0
* hospitalization.specialArrangement ..0 N
* hospitalization.specialArrangement ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.specialArrangement ^extension[=].valueCode = #4.0.0
* hospitalization.destination ..0 N
* hospitalization.destination ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.destination ^extension[=].valueCode = #4.0.0
* hospitalization.dischargeDisposition 1.. MS N
* hospitalization.dischargeDisposition from $BRMotivoDesfecho-1.0 (required)
* hospitalization.dischargeDisposition ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.dischargeDisposition ^extension[=].valueCode = #4.0.0
* hospitalization.dischargeDisposition ^short = "Motivo de Desfecho"
* hospitalization.dischargeDisposition ^definition = "Caracteriza o motivo de conclusão total ou parcial do Contato Assistencial."
* hospitalization.dischargeDisposition ^alias[0] = "Desfecho da Internação"
* hospitalization.dischargeDisposition ^binding.description = "Motivo do Desfecho"
* hospitalization.dischargeDisposition.coding 1..1 N
* hospitalization.dischargeDisposition.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.dischargeDisposition.coding ^extension[=].valueCode = #4.0.0
* hospitalization.dischargeDisposition.coding.system 1.. N
* hospitalization.dischargeDisposition.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.dischargeDisposition.coding.system ^extension[=].valueCode = #4.0.0
* hospitalization.dischargeDisposition.coding.code 1.. N
* hospitalization.dischargeDisposition.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.dischargeDisposition.coding.code ^extension[=].valueCode = #4.0.0
* hospitalization.dischargeDisposition.coding.display ..0 N
* hospitalization.dischargeDisposition.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.dischargeDisposition.coding.display ^extension[=].valueCode = #4.0.0
* hospitalization.dischargeDisposition.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* hospitalization.dischargeDisposition.coding.display ^extension[=].valueBoolean = true
* hospitalization.dischargeDisposition.coding.userSelected ..0 N
* hospitalization.dischargeDisposition.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.dischargeDisposition.coding.userSelected ^extension[=].valueCode = #4.0.0
* hospitalization.dischargeDisposition.text ..0 N
* hospitalization.dischargeDisposition.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hospitalization.dischargeDisposition.text ^extension[=].valueCode = #4.0.0
* hospitalization.dischargeDisposition.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* hospitalization.dischargeDisposition.text ^extension[=].valueBoolean = true
* location ..1 MS N
* location ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location ^extension[=].valueCode = #4.0.0
* location ^short = "Local de Atendimento"
* location ^definition = "Local onde o atendimento aconteceu."
* location.location only Reference(BRLocalAtendimento)
* location.location MS N
* location.location ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location.location ^extension[=].valueCode = #4.0.0
* location.location.reference 1.. N
* location.location.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location.location.reference ^extension[=].valueCode = #4.0.0
* location.location.type ..0 N
* location.location.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location.location.type ^extension[=].valueCode = #4.0.0
* location.location.identifier ..0 N
* location.location.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location.location.identifier ^extension[=].valueCode = #4.0.0
* location.location.display ..0 N
* location.location.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location.location.display ^extension[=].valueCode = #4.0.0
* location.location.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* location.location.display ^extension[=].valueBoolean = true
* location.status ..0 N
* location.status ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location.status ^extension[=].valueCode = #4.0.0
* location.physicalType ..0 N
* location.physicalType ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location.physicalType ^extension[=].valueCode = #4.0.0
* location.period ..0 N
* location.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location.period ^extension[=].valueCode = #4.0.0
* serviceProvider 1.. MS N
* serviceProvider only Reference(BREstabelecimentoSaude or BRPessoaJuridicaProfissionalLiberal)
* serviceProvider ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider ^extension[=].valueCode = #4.0.0
* serviceProvider ^short = "Estabelecimento de Saúde do Contato Assistencial"
* serviceProvider ^definition = "Identifica pelo número de inscrição no Cadastro Nacional de Estabelecimentos de Saúde o Estabelecimento de Saúde que realizou o Contato Assistencial."
* serviceProvider.reference ..0 N
* serviceProvider.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.reference ^extension[=].valueCode = #4.0.0
* serviceProvider.type ..0 N
* serviceProvider.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.type ^extension[=].valueCode = #4.0.0
* serviceProvider.identifier 1.. N
* serviceProvider.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.identifier ^extension[=].valueCode = #4.0.0
* serviceProvider.identifier.use ..0 N
* serviceProvider.identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.identifier.use ^extension[=].valueCode = #4.0.0
* serviceProvider.identifier.type ..0 N
* serviceProvider.identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.identifier.type ^extension[=].valueCode = #4.0.0
* serviceProvider.identifier.system 1.. N
* serviceProvider.identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.identifier.system ^extension[=].valueCode = #4.0.0
* serviceProvider.identifier.value 1.. N
* serviceProvider.identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.identifier.value ^extension[=].valueCode = #4.0.0
* serviceProvider.identifier.period ..0 N
* serviceProvider.identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.identifier.period ^extension[=].valueCode = #4.0.0
* serviceProvider.identifier.assigner ..0 N
* serviceProvider.identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.identifier.assigner ^extension[=].valueCode = #4.0.0
* serviceProvider.display ..0 N
* serviceProvider.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* serviceProvider.display ^extension[=].valueCode = #4.0.0
* serviceProvider.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* serviceProvider.display ^extension[=].valueBoolean = true
* partOf only Reference(BRContatoAssistencial)
* partOf MS N
* partOf ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* partOf ^extension[=].valueCode = #4.0.0
* partOf ^short = "Contato Assistencial anterior"
* partOf ^definition = "Permite referenciar outro Contato Assistencial que tenha dado sequência a este, como por exemplo, um indivíduo realiza, em um mesmo Estabelecimento de Saúde, um Contato Assistencial que seja de Modalidade Assistencial Atenção à Urgência/Emergência, e necessite ser internado, dando continuidade em um Contato Assistencial de Atenção Hospitalar. Neste exemplo, este elemento seria utilizado no Contato Assistencial de Atenção Hospitalar, que faria referência ao Contato Assistencial de Atenção à Urgência/Emergência."
* partOf ^type.extension.url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-hierarchy"
* partOf ^type.extension.valueBoolean = true
* partOf.reference 1.. N
* partOf.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* partOf.reference ^extension[=].valueCode = #4.0.0
* partOf.type ..0 N
* partOf.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* partOf.type ^extension[=].valueCode = #4.0.0
* partOf.identifier ..0 N
* partOf.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* partOf.identifier ^extension[=].valueCode = #4.0.0
* partOf.display ..0 N
* partOf.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* partOf.display ^extension[=].valueCode = #4.0.0
* partOf.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* partOf.display ^extension[=].valueBoolean = true