Profile: BRProcedimentoRealizado
Parent: Procedure
Id: 33f94efe-eda9-4cdf-b3a1-4b2e0d5dfba9
Title: "Procedimento Realizado"
Description: "Procedimento realizado em um indivíduo."
* ^meta.lastUpdated = "2020-04-08T11:32:29.790+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRProcedimentoRealizado-1.0"
* ^version = "1.0"
* ^status = #active
* ^date = "2020-04-08T11:32:31.3117037+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* . MS
* . ^short = "Procedimento Realizado"
* . ^definition = "Ação de saúde realizada no indivíduo."
* extension ^slicing.discriminator.type = #value
* extension ^slicing.discriminator.path = "url"
* extension ^slicing.rules = #open
* extension contains BRQuantidade named quantity 1..1 N
* extension[quantity] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* extension[quantity] ^extension[=].valueCode = #4.0.0
* extension[quantity] ^definition = "Quantidade realizada do procedimento."
* extension[quantity].value[x] only positiveInt
* extension[quantity].value[x] N
* extension[quantity].value[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* extension[quantity].value[x] ^extension[=].valueCode = #4.0.0
* identifier ..1 MS N
* identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier ^extension[=].valueCode = #4.0.0
* identifier.use ..0 N
* identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.use ^extension[=].valueCode = #4.0.0
* identifier.type 1.. N
* identifier.type = $BRTipoIdentificador#AUTH (exactly)
* identifier.type from $BRTipoIdentificadorProcedimento-1.0 (required)
* identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.type ^extension[=].valueCode = #4.0.0
* identifier.type ^short = "Tipo de Identificador"
* identifier.type ^binding.description = "Tipo de Identificador"
* identifier.type.coding 1..1 N
* identifier.type.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.type.coding ^extension[=].valueCode = #4.0.0
* identifier.type.coding.system 1.. N
* identifier.type.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.type.coding.system ^extension[=].valueCode = #4.0.0
* identifier.type.coding.code 1.. N
* identifier.type.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.type.coding.code ^extension[=].valueCode = #4.0.0
* identifier.type.coding.display ..0 N
* identifier.type.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.type.coding.display ^extension[=].valueCode = #4.0.0
* identifier.type.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* identifier.type.coding.display ^extension[=].valueBoolean = true
* identifier.type.coding.userSelected ..0 N
* identifier.type.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.type.coding.userSelected ^extension[=].valueCode = #4.0.0
* identifier.type.text ..0 N
* identifier.type.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.type.text ^extension[=].valueCode = #4.0.0
* identifier.type.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* identifier.type.text ^extension[=].valueBoolean = true
* identifier.system ..0 N
* identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.system ^extension[=].valueCode = #4.0.0
* identifier.value 1.. N
* identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.value ^extension[=].valueCode = #4.0.0
* identifier.value ^short = "Código de Autorização"
* identifier.value ^definition = "Código do identificador da permissão para a realização do procedimento."
* identifier.period ..0 N
* identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.period ^extension[=].valueCode = #4.0.0
* identifier.assigner ..0 N
* identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier.assigner ^extension[=].valueCode = #4.0.0
* instantiatesCanonical ..0 N
* instantiatesCanonical ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* instantiatesCanonical ^extension[=].valueCode = #4.0.0
* instantiatesUri ..0 N
* instantiatesUri ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* instantiatesUri ^extension[=].valueCode = #4.0.0
* basedOn ..0 N
* basedOn ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* basedOn ^extension[=].valueCode = #4.0.0
* partOf ..0 N
* partOf ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* partOf ^extension[=].valueCode = #4.0.0
* status MS N
* status from $BREstadoEvento-1.0 (required)
* status ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* status ^extension[=].valueCode = #4.0.0
* status ^short = "Estado do Procedimento"
* status ^definition = "Estado da realização do procedimento."
* status ^comment = "Na primeira fase da RNDS somente serão informados procedimentos realizados ou cancelados (com erro), portanto, somente o valor completed será aceito."
* status ^binding.description = "Estado do Evento"
* statusReason ..0 N
* statusReason ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* statusReason ^extension[=].valueCode = #4.0.0
* category ..0 N
* category ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category ^extension[=].valueCode = #4.0.0
* code 1.. MS N
* code from $BRProcedimentosNacionais-1.0 (required)
* code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code ^extension[=].valueCode = #4.0.0
* code ^short = "Procedimento Realizado"
* code ^definition = "Identificação do procedimento que foi realizado."
* code ^binding.description = "Classificações Nacionais de Procedimentos"
* code.coding 1..1 N
* code.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding ^extension[=].valueCode = #4.0.0
* code.coding.system 1.. N
* code.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.system ^extension[=].valueCode = #4.0.0
* code.coding.code 1.. N
* code.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.code ^extension[=].valueCode = #4.0.0
* code.coding.display ..0 N
* code.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.display ^extension[=].valueCode = #4.0.0
* code.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* code.coding.display ^extension[=].valueBoolean = true
* code.coding.userSelected ..0 N
* code.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.userSelected ^extension[=].valueCode = #4.0.0
* code.text ..0 N
* code.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.text ^extension[=].valueCode = #4.0.0
* code.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* code.text ^extension[=].valueBoolean = true
* subject only Reference(BRIndividuo)
* subject MS N
* subject ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject ^extension[=].valueCode = #4.0.0
* subject ^short = "Indivíduo"
* subject ^definition = "Indivíduo em que o procedimento foi realizado."
* subject.extension ^slicing.discriminator.type = #value
* subject.extension ^slicing.discriminator.path = "url"
* subject.extension ^slicing.rules = #open
* subject.extension[unidentifiedPatient] only BRIndividuoNaoIdentificado
* subject.extension[unidentifiedPatient] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient] ^sliceName = "unidentifiedPatient"
* subject.extension[unidentifiedPatient] ^short = "Dados do Indivíduo Não Identificado"
* subject.extension[unidentifiedPatient] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[gender] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[gender] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[gender] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[gender] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[gender] ^sliceName = "gender"
* subject.extension[unidentifiedPatient].extension[gender] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[birthYear] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[birthYear] ^sliceName = "birthYear"
* subject.extension[unidentifiedPatient].extension[birthYear] ^mustSupport = true
* subject.extension[unidentifiedPatient].extension[reason] ^extension[0].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-standards-status"
* subject.extension[unidentifiedPatient].extension[reason] ^extension[=].valueCode = #normative
* subject.extension[unidentifiedPatient].extension[reason] ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.extension[unidentifiedPatient].extension[reason] ^extension[=].valueCode = #4.0.0
* subject.extension[unidentifiedPatient].extension[reason] ^sliceName = "reason"
* subject.extension[unidentifiedPatient].extension[reason] ^mustSupport = true
* subject.reference ..0 N
* subject.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.reference ^extension[=].valueCode = #4.0.0
* subject.type ..0 N
* subject.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.type ^extension[=].valueCode = #4.0.0
* subject.type ^short = "Recurso do sujeito"
* subject.type ^definition = "Indica o recurso FHIR que será utilizado na RNDS para identificar de forma direta o sujeito que do procedimento."
* subject.identifier.use ..0 N
* subject.identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.use ^extension[=].valueCode = #4.0.0
* subject.identifier.type ..0 N
* subject.identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.type ^extension[=].valueCode = #4.0.0
* subject.identifier.system 1.. N
* subject.identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.system ^extension[=].valueCode = #4.0.0
* subject.identifier.value 1.. N
* subject.identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.value ^extension[=].valueCode = #4.0.0
* subject.identifier.period ..0 N
* subject.identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.period ^extension[=].valueCode = #4.0.0
* subject.identifier.assigner ..0 N
* subject.identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.assigner ^extension[=].valueCode = #4.0.0
* subject.display ..0 N
* subject.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.display ^extension[=].valueCode = #4.0.0
* subject.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* subject.display ^extension[=].valueBoolean = true
* encounter ..0 N
* encounter ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* encounter ^extension[=].valueCode = #4.0.0
* performed[x] 1.. MS N
* performed[x] only dateTime
* performed[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performed[x] ^extension[=].valueCode = #4.0.0
* performed[x] ^short = "Momento da Realização do Procedimento"
* performed[x] ^definition = "Competência (mês e ano), data ou data e hora que o procedimento foi realizado."
* recorder ..0 N
* recorder ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* recorder ^extension[=].valueCode = #4.0.0
* asserter ..0 N
* asserter ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* asserter ^extension[=].valueCode = #4.0.0
* performer contains practitioner 1..* MS N
* performer[practitioner] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner] ^extension[=].valueCode = #4.0.0
* performer[practitioner] ^short = "Executante"
* performer[practitioner] ^definition = "Permite identificar:\r\n- o(s) profissional(is) lotado(s) em um Estabelecimento de Saúde inscrito no CNES que realizou(aram) o procedimento, sua(s) respectiva(s) ocupação(ões) e o Estabelecimento de Saúde Terceiro (quando aplicável) - OPÇÃO PREFERENCIAL;\r\n- apenas a(s) ocupação(ões) do(s) profisssional(is) que realizou(aram) o procedimento, seu(s) Estabelecimento(s) de Saúde inscrito(s) no CNES e o Estabelecimento de Saúde Terceiro (quando aplicável) - OPÇÃO ALTERNATIVA quando o profissional não está inscrito no respectivo CNES ou o modelo de origem não disponha dessa informação;\r\n- apenas a(s) ocupação(ões) do(s) profisssional(is) que realizou(aram) o procedimento, a(s) organização(ões) identificada(s) pelo CNPJ (pessoa jurídica) ou CPF (profissional liberal) onde o procedimento foi realizado e o Estabelecimento de Saúde Terceiro (quando aplicável) - ÚLTIMA OPÇÃO, utilizar apenas quando o estabelecimento de saúde não possui inscrição no CNES ou o modelo de origem não disponha dessa informação."
* performer[practitioner].extension ^slicing.discriminator.type = #value
* performer[practitioner].extension ^slicing.discriminator.path = "url"
* performer[practitioner].extension ^slicing.rules = #open
* performer[practitioner].extension contains BRIdentificacaoEquipe named healthcareTeam 0..1 MS N
* performer[practitioner].extension[healthcareTeam] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].extension[healthcareTeam] ^extension[=].valueCode = #4.0.0
* performer[practitioner].extension[healthcareTeam] ^short = "Equipe de Saúde"
* performer[practitioner].extension[healthcareTeam] ^definition = "Identifica a equipe de saúde que realizou o procedimento utilizando o Identificador Nacional de Equipes."
* performer[practitioner].function 1.. MS N
* performer[practitioner].function from $BROcupacao-1.0 (required)
* performer[practitioner].function ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].function ^extension[=].valueCode = #4.0.0
* performer[practitioner].function ^short = "Ocupação do Profissional"
* performer[practitioner].function ^definition = "Atividade desempenhada pelo profissional que realizou o procedimento."
* performer[practitioner].function ^binding.extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName"
* performer[practitioner].function ^binding.extension.valueString = "ProcedurePerformerRole"
* performer[practitioner].function ^binding.description = "Ocupação Profissional"
* performer[practitioner].function.coding 1..1 N
* performer[practitioner].function.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].function.coding ^extension[=].valueCode = #4.0.0
* performer[practitioner].function.coding.system 1.. N
* performer[practitioner].function.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].function.coding.system ^extension[=].valueCode = #4.0.0
* performer[practitioner].function.coding.code 1.. N
* performer[practitioner].function.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].function.coding.code ^extension[=].valueCode = #4.0.0
* performer[practitioner].function.coding.display ..0 N
* performer[practitioner].function.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].function.coding.display ^extension[=].valueCode = #4.0.0
* performer[practitioner].function.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* performer[practitioner].function.coding.display ^extension[=].valueBoolean = true
* performer[practitioner].function.coding.userSelected ..0 N
* performer[practitioner].function.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].function.coding.userSelected ^extension[=].valueCode = #4.0.0
* performer[practitioner].function.text ..0 N
* performer[practitioner].function.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].function.text ^extension[=].valueCode = #4.0.0
* performer[practitioner].function.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* performer[practitioner].function.text ^extension[=].valueBoolean = true
* performer[practitioner].actor only Reference(BRLotacaoProfissional or BREstabelecimentoSaude or BRPessoaJuridicaProfissionalLiberal)
* performer[practitioner].actor MS N
* performer[practitioner].actor ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor ^short = "Profissional"
* performer[practitioner].actor.reference ..0 N
* performer[practitioner].actor.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.reference ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.type ..0 N
* performer[practitioner].actor.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.type ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.identifier 1.. N
* performer[practitioner].actor.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.identifier ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.identifier.use ..0 N
* performer[practitioner].actor.identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.identifier.use ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.identifier.type ..0 N
* performer[practitioner].actor.identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.identifier.type ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.identifier.system 1.. N
* performer[practitioner].actor.identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.identifier.system ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.identifier.value 1.. N
* performer[practitioner].actor.identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.identifier.value ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.identifier.period ..0 N
* performer[practitioner].actor.identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.identifier.period ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.identifier.assigner ..0 N
* performer[practitioner].actor.identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.identifier.assigner ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.display ..0 N
* performer[practitioner].actor.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].actor.display ^extension[=].valueCode = #4.0.0
* performer[practitioner].actor.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* performer[practitioner].actor.display ^extension[=].valueBoolean = true
* performer[practitioner].onBehalfOf only Reference(BREstabelecimentoSaude or BRPessoaJuridicaProfissionalLiberal)
* performer[practitioner].onBehalfOf MS N
* performer[practitioner].onBehalfOf ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf ^short = "Estabelecimento de Saúde Terceiro"
* performer[practitioner].onBehalfOf ^definition = "Estabelecimento de Saúde que realizou o procedimento como serviço terceiro do Estabelecimento de Saúde onde o procediemento foi realizado."
* performer[practitioner].onBehalfOf.reference ..0 N
* performer[practitioner].onBehalfOf.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.reference ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.type ..0 N
* performer[practitioner].onBehalfOf.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.type ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.identifier 1.. N
* performer[practitioner].onBehalfOf.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.identifier ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.identifier.use ..0 N
* performer[practitioner].onBehalfOf.identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.identifier.use ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.identifier.type ..0 N
* performer[practitioner].onBehalfOf.identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.identifier.type ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.identifier.system 1.. N
* performer[practitioner].onBehalfOf.identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.identifier.system ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.identifier.value 1.. N
* performer[practitioner].onBehalfOf.identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.identifier.value ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.identifier.period ..0 N
* performer[practitioner].onBehalfOf.identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.identifier.period ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.identifier.assigner ..0 N
* performer[practitioner].onBehalfOf.identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.identifier.assigner ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.display ..0 N
* performer[practitioner].onBehalfOf.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[practitioner].onBehalfOf.display ^extension[=].valueCode = #4.0.0
* performer[practitioner].onBehalfOf.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* performer[practitioner].onBehalfOf.display ^extension[=].valueBoolean = true
* location ..0 N
* location ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* location ^extension[=].valueCode = #4.0.0
* reasonCode ..0 N
* reasonCode ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonCode ^extension[=].valueCode = #4.0.0
* reasonReference ..0 N
* reasonReference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* reasonReference ^extension[=].valueCode = #4.0.0
* bodySite ..0 N
* bodySite ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* bodySite ^extension[=].valueCode = #4.0.0
* outcome ..0 N
* outcome ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* outcome ^extension[=].valueCode = #4.0.0
* report ..0 N
* report ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* report ^extension[=].valueCode = #4.0.0
* complication ..0 N
* complication ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* complication ^extension[=].valueCode = #4.0.0
* complicationDetail ..0 N
* complicationDetail ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* complicationDetail ^extension[=].valueCode = #4.0.0
* followUp ..0 N
* followUp ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* followUp ^extension[=].valueCode = #4.0.0
* note MS N
* note ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note ^extension[=].valueCode = #4.0.0
* note ^short = "Resultado ou observações do procedimento"
* note ^definition = "Possibilita realizar anotações acerca do desfecho e observações do procedimento."
* note.author[x] ..0 N
* note.author[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note.author[x] ^extension[=].valueCode = #4.0.0
* note.time ..0 N
* note.time ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note.time ^extension[=].valueCode = #4.0.0
* note.text MS N
* note.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note.text ^extension[=].valueCode = #4.0.0
* focalDevice ..0 N
* focalDevice ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* focalDevice ^extension[=].valueCode = #4.0.0
* usedReference ..0 N
* usedReference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* usedReference ^extension[=].valueCode = #4.0.0
* usedCode ..0 N
* usedCode ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* usedCode ^extension[=].valueCode = #4.0.0