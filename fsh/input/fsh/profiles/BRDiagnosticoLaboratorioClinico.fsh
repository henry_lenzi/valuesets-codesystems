Profile: BRDiagnosticoLaboratorioClinico
Parent: Observation
Id: 10adbc3a-b1e9-4620-8c02-6f530e2caae0
Title: "Diagnóstico em Laboratório Clínico"
Description: "Exame ou teste realizado em laboratório com finalidade diagnóstica ou investigativa."
* ^meta.lastUpdated = "2020-03-30T18:35:39.659+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRDiagnosticoLaboratorioClinico-1.0"
* ^version = "1.0"
* ^date = "2020-03-30T18:35:35.8568283+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* . MS
* . ^definition = "Exame ou teste realizado em laboratório com finalidade diagnóstica ou investigativa."
* identifier ..0 N
* identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* identifier ^extension[=].valueCode = #4.0.0
* basedOn ..0 N
* basedOn ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* basedOn ^extension[=].valueCode = #4.0.0
* partOf ..0 N
* partOf ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* partOf ^extension[=].valueCode = #4.0.0
* status N
* status from $BREstadoObservacao-1.0 (required)
* status ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* status ^extension[=].valueCode = #4.0.0
* status ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-display-hint"
* status ^extension[=].valueString = "default: final"
* status ^short = "Estado da Observação"
* status ^definition = "O estado do valor do resultado."
* status ^binding.extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName"
* status ^binding.extension.valueString = "ObservationStatus"
* status ^binding.description = "Estado da Observação"
* category 1..1 MS N
* category from $BRCategoriaExame-1.0 (required)
* category ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category ^extension[=].valueCode = #4.0.0
* category ^short = "Categoria do Exame"
* category ^definition = "Categoriza o exame ou teste utilizando os subgrupos do grupo 02 - Procedimentos com finalidade diagnóstica da Tabela SUS."
* category ^binding.extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName"
* category ^binding.extension.valueString = "ObservationCategory"
* category ^binding.description = "Categoria do Exame"
* category.coding 1..1 N
* category.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category.coding ^extension[=].valueCode = #4.0.0
* category.coding.system 1.. N
* category.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category.coding.system ^extension[=].valueCode = #4.0.0
* category.coding.code 1.. N
* category.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category.coding.code ^extension[=].valueCode = #4.0.0
* category.coding.display ..0 N
* category.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category.coding.display ^extension[=].valueCode = #4.0.0
* category.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* category.coding.display ^extension[=].valueBoolean = true
* category.coding.userSelected ..0 N
* category.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category.coding.userSelected ^extension[=].valueCode = #4.0.0
* category.text ..0 N
* category.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* category.text ^extension[=].valueCode = #4.0.0
* category.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* category.text ^extension[=].valueBoolean = true
* code MS N
* code from $BRNomeExame-1.0 (required)
* code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code ^extension[=].valueCode = #4.0.0
* code ^short = "Nome do Exame"
* code ^definition = "Classifica o exame laboratorial realizado."
* code ^binding.extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName"
* code ^binding.extension.valueString = "ObservationCode"
* code ^binding.description = "Nome do Exame"
* code.coding 1..1 N
* code.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding ^extension[=].valueCode = #4.0.0
* code.coding.system 1.. N
* code.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.system ^extension[=].valueCode = #4.0.0
* code.coding.code 1.. N
* code.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.code ^extension[=].valueCode = #4.0.0
* code.coding.display ..0 N
* code.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.display ^extension[=].valueCode = #4.0.0
* code.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* code.coding.display ^extension[=].valueBoolean = true
* code.coding.userSelected ..0 N
* code.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.coding.userSelected ^extension[=].valueCode = #4.0.0
* code.text ..0 N
* code.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* code.text ^extension[=].valueCode = #4.0.0
* code.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* code.text ^extension[=].valueBoolean = true
* subject 1.. MS N
* subject only Reference(BRIndividuo)
* subject ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject ^extension[=].valueCode = #4.0.0
* subject.reference ..0 N
* subject.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.reference ^extension[=].valueCode = #4.0.0
* subject.type ..0 N
* subject.type = "Patient" (exactly)
* subject.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.type ^extension[=].valueCode = #4.0.0
* subject.identifier 1.. N
* subject.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier ^extension[=].valueCode = #4.0.0
* subject.identifier.use ..0 N
* subject.identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.use ^extension[=].valueCode = #4.0.0
* subject.identifier.type ..0 N
* subject.identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.type ^extension[=].valueCode = #4.0.0
* subject.identifier.system 1.. N
* subject.identifier.system = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRIndividuo-1.0" (exactly)
* subject.identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.system ^extension[=].valueCode = #4.0.0
* subject.identifier.value 1.. N
* subject.identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.value ^extension[=].valueCode = #4.0.0
* subject.identifier.period ..0 N
* subject.identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.period ^extension[=].valueCode = #4.0.0
* subject.identifier.assigner ..0 N
* subject.identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.identifier.assigner ^extension[=].valueCode = #4.0.0
* subject.display ..0 N
* subject.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* subject.display ^extension[=].valueCode = #4.0.0
* subject.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* subject.display ^extension[=].valueBoolean = true
* focus ..0 TU
* focus ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* focus ^extension[=].valueCode = #4.0.0
* encounter ..0 N
* encounter ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* encounter ^extension[=].valueCode = #4.0.0
* effective[x] ..0 N
* effective[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* effective[x] ^extension[=].valueCode = #4.0.0
* issued 1.. N
* issued ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* issued ^extension[=].valueCode = #4.0.0
* issued ^short = "Data/Hora do Resultado"
* issued ^definition = "Data ou data e hora que o resultado foi liberado."
* performer 1..1 MS N
* performer ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer ^extension[=].valueCode = #4.0.0
* performer ^slicing.discriminator.type = #value
* performer ^slicing.discriminator.path = "identifier.system"
* performer ^slicing.rules = #open
* performer ^short = "Responsável pelo Resultado do Exame"
* performer ^definition = "Profissional e/ou Estabelecimento de Saúde responsável pelo resultado do exame."
* performer contains
    estabelecimentoSaude 0..1 MS N and
    pessoaJuridicaProfissionalLiberal 0..1 MS N and
    lotacaoProfissional 0..1 MS N
* performer[estabelecimentoSaude] only Reference(BREstabelecimentoSaude)
* performer[estabelecimentoSaude] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude] ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].reference ..0 N
* performer[estabelecimentoSaude].reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].reference ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].type ..0 N
* performer[estabelecimentoSaude].type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].type ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].identifier 1.. N
* performer[estabelecimentoSaude].identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].identifier ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].identifier.use ..0 N
* performer[estabelecimentoSaude].identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].identifier.use ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].identifier.type ..0 N
* performer[estabelecimentoSaude].identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].identifier.type ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].identifier.system 1.. N
* performer[estabelecimentoSaude].identifier.system = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BREstabelecimentoSaude-1.0" (exactly)
* performer[estabelecimentoSaude].identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].identifier.system ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].identifier.value 1.. N
* performer[estabelecimentoSaude].identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].identifier.value ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].identifier.period ..0 N
* performer[estabelecimentoSaude].identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].identifier.period ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].identifier.assigner ..0 N
* performer[estabelecimentoSaude].identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].identifier.assigner ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].display ..0 N
* performer[estabelecimentoSaude].display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[estabelecimentoSaude].display ^extension[=].valueCode = #4.0.0
* performer[estabelecimentoSaude].display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* performer[estabelecimentoSaude].display ^extension[=].valueBoolean = true
* performer[pessoaJuridicaProfissionalLiberal] only Reference(BRPessoaJuridicaProfissionalLiberal)
* performer[pessoaJuridicaProfissionalLiberal] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal] ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].reference ..0 N
* performer[pessoaJuridicaProfissionalLiberal].reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].reference ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].type ..0 N
* performer[pessoaJuridicaProfissionalLiberal].type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].type ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].identifier 1.. N
* performer[pessoaJuridicaProfissionalLiberal].identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].identifier ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].identifier.use ..0 N
* performer[pessoaJuridicaProfissionalLiberal].identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].identifier.use ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].identifier.type ..0 N
* performer[pessoaJuridicaProfissionalLiberal].identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].identifier.type ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].identifier.system 1.. N
* performer[pessoaJuridicaProfissionalLiberal].identifier.system = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRPessoaJuridicaProfissionalLiberal-1.0" (exactly)
* performer[pessoaJuridicaProfissionalLiberal].identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].identifier.system ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].identifier.value 1.. N
* performer[pessoaJuridicaProfissionalLiberal].identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].identifier.value ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].identifier.period ..0 N
* performer[pessoaJuridicaProfissionalLiberal].identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].identifier.period ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].identifier.assigner ..0 N
* performer[pessoaJuridicaProfissionalLiberal].identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].identifier.assigner ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].display ..0 N
* performer[pessoaJuridicaProfissionalLiberal].display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[pessoaJuridicaProfissionalLiberal].display ^extension[=].valueCode = #4.0.0
* performer[pessoaJuridicaProfissionalLiberal].display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* performer[pessoaJuridicaProfissionalLiberal].display ^extension[=].valueBoolean = true
* performer[lotacaoProfissional] only Reference(BRLotacaoProfissional)
* performer[lotacaoProfissional] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional] ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].reference ..0 N
* performer[lotacaoProfissional].reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].reference ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].type ..0 N
* performer[lotacaoProfissional].type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].type ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].identifier 1.. N
* performer[lotacaoProfissional].identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].identifier ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].identifier.use ..0 N
* performer[lotacaoProfissional].identifier.use ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].identifier.use ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].identifier.type ..0 N
* performer[lotacaoProfissional].identifier.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].identifier.type ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].identifier.system 1.. N
* performer[lotacaoProfissional].identifier.system = "http://www.saude.gov.br/fhir/r4/StructureDefinition/BRLotacaoProfissional-1.0" (exactly)
* performer[lotacaoProfissional].identifier.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].identifier.system ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].identifier.value 1.. N
* performer[lotacaoProfissional].identifier.value ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].identifier.value ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].identifier.period ..0 N
* performer[lotacaoProfissional].identifier.period ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].identifier.period ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].identifier.assigner ..0 N
* performer[lotacaoProfissional].identifier.assigner ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].identifier.assigner ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].display ..0 N
* performer[lotacaoProfissional].display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* performer[lotacaoProfissional].display ^extension[=].valueCode = #4.0.0
* performer[lotacaoProfissional].display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* performer[lotacaoProfissional].display ^extension[=].valueBoolean = true
* value[x] 1.. MS N
* value[x] only Quantity or CodeableConcept
* value[x] from $BRResultadoQualitativoExame-1.0 (required)
* value[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* value[x] ^extension[=].valueCode = #4.0.0
* value[x] ^short = "Valor do Resultado"
* value[x] ^definition = "Pode ser um valor quantitativo, utilizando o elemento Quantity, ou um valor qualitativo, utilizando Codeable Concept.E usar"
* value[x] ^binding.description = "Resultado Qualitativo do Exame"
* dataAbsentReason ..0 N
* dataAbsentReason ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* dataAbsentReason ^extension[=].valueCode = #4.0.0
* interpretation ..1 MS N
* interpretation from $BRResultadoQualitativoExame-1.0 (required)
* interpretation ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* interpretation ^extension[=].valueCode = #4.0.0
* interpretation ^short = "Interpretação Qualitativa"
* interpretation ^definition = "Interpretação qualitativa de um resultado quantitativo."
* interpretation ^binding.extension.url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName"
* interpretation ^binding.extension.valueString = "ObservationInterpretation"
* interpretation ^binding.description = "Interperetação Qualitativa do Exame"
* interpretation.coding 1..1 N
* interpretation.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* interpretation.coding ^extension[=].valueCode = #4.0.0
* interpretation.coding.system 1.. N
* interpretation.coding.system ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* interpretation.coding.system ^extension[=].valueCode = #4.0.0
* interpretation.coding.code 1.. N
* interpretation.coding.code ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* interpretation.coding.code ^extension[=].valueCode = #4.0.0
* interpretation.coding.display ..0 N
* interpretation.coding.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* interpretation.coding.display ^extension[=].valueCode = #4.0.0
* interpretation.coding.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* interpretation.coding.display ^extension[=].valueBoolean = true
* interpretation.coding.userSelected ..0 N
* interpretation.coding.userSelected ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* interpretation.coding.userSelected ^extension[=].valueCode = #4.0.0
* interpretation.text ..0 N
* interpretation.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* interpretation.text ^extension[=].valueCode = #4.0.0
* interpretation.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* interpretation.text ^extension[=].valueBoolean = true
* note MS N
* note ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note ^extension[=].valueCode = #4.0.0
* note ^short = "Notas"
* note ^definition = "Comentários sobre os resultados dos exames."
* note.author[x] ..0 N
* note.author[x] ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note.author[x] ^extension[=].valueCode = #4.0.0
* note.time ..0 N
* note.time ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* note.time ^extension[=].valueCode = #4.0.0
* bodySite ..0 N
* bodySite ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* bodySite ^extension[=].valueCode = #4.0.0
* method 1.. MS N
* method ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* method ^extension[=].valueCode = #4.0.0
* method ^short = "Método"
* method ^definition = "Método empregado no exame realizado."
* method.coding ..0 N
* method.coding ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* method.coding ^extension[=].valueCode = #4.0.0
* method.text MS N
* method.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* method.text ^extension[=].valueCode = #4.0.0
* method.text ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* method.text ^extension[=].valueBoolean = true
* specimen 1.. MS N
* specimen only Reference(BRAmostraBiologica)
* specimen ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* specimen ^extension[=].valueCode = #4.0.0
* specimen ^short = "Amostra"
* specimen ^definition = "A amostra que foi usada quando esta observação foi realizada."
* specimen.reference 1.. N
* specimen.reference ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* specimen.reference ^extension[=].valueCode = #4.0.0
* specimen.type ..0 N
* specimen.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* specimen.type ^extension[=].valueCode = #4.0.0
* specimen.identifier ..0 N
* specimen.identifier ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* specimen.identifier ^extension[=].valueCode = #4.0.0
* specimen.display ..0 N
* specimen.display ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* specimen.display ^extension[=].valueCode = #4.0.0
* specimen.display ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-translatable"
* specimen.display ^extension[=].valueBoolean = true
* device ..0 N
* device ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* device ^extension[=].valueCode = #4.0.0
* referenceRange 1..1 MS N
* referenceRange ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* referenceRange ^extension[=].valueCode = #4.0.0
* referenceRange ^short = "Valor de Referência"
* referenceRange ^definition = "Valores de referência para apoiar na interpretação do resultado."
* referenceRange.low ..0 N
* referenceRange.low ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* referenceRange.low ^extension[=].valueCode = #4.0.0
* referenceRange.high ..0 N
* referenceRange.high ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* referenceRange.high ^extension[=].valueCode = #4.0.0
* referenceRange.type ..0 N
* referenceRange.type ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* referenceRange.type ^extension[=].valueCode = #4.0.0
* referenceRange.appliesTo ..0 N
* referenceRange.appliesTo ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* referenceRange.appliesTo ^extension[=].valueCode = #4.0.0
* referenceRange.age ..0 N
* referenceRange.age ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* referenceRange.age ^extension[=].valueCode = #4.0.0
* referenceRange.text 1.. N
* referenceRange.text ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* referenceRange.text ^extension[=].valueCode = #4.0.0
* hasMember ..0 N
* hasMember ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* hasMember ^extension[=].valueCode = #4.0.0
* derivedFrom ..0 N
* derivedFrom ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* derivedFrom ^extension[=].valueCode = #4.0.0
* component ..0 N
* component ^extension[1].url = "http://hl7.org/fhir/StructureDefinition/structuredefinition-normative-version"
* component ^extension[=].valueCode = #4.0.0