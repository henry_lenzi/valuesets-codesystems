CodeSystem: Caráter_de_Atendimento
Id: 102cd026-c627-48b8-b584-aae681ae2d1d
Description: "Terminologia que classifica a prioridade de realização de um Contato Assistencial."
* ^name = "Caráter de Atendimento"
* ^meta.lastUpdated = "2020-03-11T11:56:57.326+00:00"
* ^language = #pt-BR
* ^url = "http://www.saude.gov.br/fhir/r4/CodeSystem/BRCaraterAtendimento"
* ^version = "201701A"
* ^status = #active
* ^experimental = false
* ^date = "2020-03-11T11:57:17.2973841+00:00"
* ^publisher = "Ministério da Saúde do Brasil"
* ^content = #complete
* #01 "Eletivo"
* #02 "Urgência"